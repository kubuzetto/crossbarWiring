import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class wiring {
 	
	public static void main (String [] args) {
		int mode = -1; int count = -1; String filename = "";
		if (args.length==2 && args[0].equalsIgnoreCase("--sanity") && Integer.parseInt(args[1])>0) {
			mode = 0;
			count = Integer.parseInt(args[1]);
		} else if (args.length==2 && args[0].equalsIgnoreCase("--console") && Integer.parseInt(args[1])>0) {
			mode = 1;
			count = Integer.parseInt(args[1]);
		} else if (args.length==3 && args[0].equalsIgnoreCase("--svg") && Integer.parseInt(args[1])>0) {
			mode = 2;
			count = Integer.parseInt(args[1]);
			filename = args[2];
		}
			
		switch (mode) {
			case 0: // Sanity check
				sanityCheck(count);
				break;
			case 1: // ASCII console print
				createASCII (count);
				break;
			case 2: // SVG output
				createSVG (count, filename);
				break;
			default: // Error in inputs
				System.out.println("wiring.java: Crossbar wiring solution tool (Devrim Sahin)\r\n"+
					"\r\n"+
					"Usage:\r\n"+
						"\t1. Sanity check mode:\r\n"+
						"\t\tConstruct all the wiring schemes up to n, checks correctness\r\n"+
						"\t\tjava wiring --sanity <n>\r\n"+
						"\t\t(n: count limit)+\r\n"+
						"\r\n"+
						"\t2. Console output mode:\r\n"+
						"\t\tConstructs the wiring scheme for n inputs, prints the wiring into console\r\n"+
						"\t\tjava wiring --console <n>\r\n"+
						"\t\t(n: input count)\r\n"+
						"\r\n"+
						"\t3. SVG output mode:\r\n"+
						"\t\tConstructs the wiring scheme for n inputs, draws the wiring into an SVG file (Recommended)\r\n"+
						"\t\tjava wiring --svg <n> <f>\r\n"+
						"\t\t(n: input count)\r\n"+
						"\t\t(f: filename)\r\n");
		}
	}
	
	// Main algorithm
	
	public static int[] constructLevel (int count, int stepSize) {
		boolean isFinal = (2*stepSize == count);
		if (isFinal) {
			int[] conn=new int[count];
			for (int i=0;i<stepSize; i++) {
				conn[i]=i+stepSize;
				conn[i+stepSize] = i;
			}
			return conn;
		}
		int[] conn = new int[2*count];
		
		for (int i=0; i<count; i++) {
			int k1=i+((i/stepSize) % 2)*count;
			int k2=k1+stepSize;
			if (k2>=2*count) k2-=count;
			conn[k1] = k2;
			conn[k2] = k1;
		}
		return conn;
	}
	
	
	// OPTION 1: Do a sanity check (check if the entire adjacency matrix is covered)
	
	public static void sanityCheck (int countLimit) {
		System.out.println("Beginning sanity check...");
		for (int count=1; count<countLimit; count++)
			if (!__sanityCheck(count))
				System.out.println("Sanity check for count "+count+" failed!");
		System.out.println("Completed.");
	}
	
	private static boolean __sanityCheck (int count) {
		int[][] adjMat = new int[count][count];
		for (int i=1; i<=count/2; i++) {
			int[] h = constructLevel (count, i);
			for (int m=0; m<h.length; m++)
				adjMat[m%count][h[m]%count]+=1;
		}
		for (int i=0; i<count; i++)
			for (int j=0; j<count; j++)
				if (i!=j && adjMat[i][j]!=1)
					return false;
		return true;
	}
	
	// OPTION 2: Print to console as ASCII text
	
	public static void createASCII (int count) {
		for (int i=1; i<=count/2; i++)
			System.out.print(printLevel(constructLevel (count, i)));
	}
	
	private static String printLevel (int[] p) {
		boolean isFinal = (2*p[0] == p.length);
		int[][] matrx = new int[p.length][isFinal?(p.length/2):(p.length/4)];
		int[] occpLevels = new int[matrx[0].length];
		for (int i=0; i<p.length; i++) {
			if (p[i]<i) continue;
			int levl = 0;
			for (int j=occpLevels.length-1; j>=0; j--) {
				occpLevels[j] -= 1;
				if (occpLevels[j] <= 0) {
					occpLevels[j] = 0;
					levl = j;
				}
			}
			occpLevels [levl] = p[i]-i;
			
			matrx[i][levl] = 2;
			matrx[p[i]][levl] = 3;
			for (int j=i+1; j<p[i];j++) matrx[j][levl] = 1;
		}

		String s = ""; int a=0;
		int h=0; s+="  \t"; for (@SuppressWarnings("unused") int m: matrx[0]) s+=" "+(((++h)<10)?" ":(h/10)); s+=" \n";
		    h=0; s+="  \t"; for (@SuppressWarnings("unused") int m: matrx[0]) s+=" "+((++h)%10); s+=" \n";
		for (int[] k:matrx) {
			s+=((a++)%((isFinal)?p.length:(p.length/2)))+"\t"; for (int m:k) s+="-"+((m==0)?"-":((m==1)?"|":"*")); s+="-\n";
			s+="  \t"; for (int m:k) s+=" "+((m==0 | m==3)?" ":"|"); s+=" \n";
		}
		return s;
	}
	
	// OPTION 3: Save as SVG figure
	
	public static void createSVG (int count, String filename) {
		ArrayList <float[]> lines = new ArrayList<float[]>();
		float xOffset = 20;
		for (int i=1; i<=count/2; i++) {
			printLevelSVG(constructLevel(count, i), lines, 0, xOffset, 10);
			xOffset +=(6+count/2)*10;
		}
		
		int width = 1; int height=1;
		for (float[] f: lines)
			for (int i=0; i<f.length; i+=2) {
				if (width<f[i]) width = (int) Math.ceil(f[i]);
				if (height<f[i+1]) height = (int) Math.ceil(f[i+1]);
			}
		width+=20;
		height+=20;
		BufferedWriter writer = null;
	    try {
			writer = new BufferedWriter(new FileWriter(filename));
		    writer.write(getSVGPrefix(width, height));
		    int i=0;
		    for (float[] p: lines)
		    	writer.write(getAbsPathText(p, false, "path"+(i++)));
			for (int h=0; h<count*(count-1); h++) {
				writer.write(getLabelText((6+count/2)*10*(h/(2*count))+5, (h%(2*count))*10+15, h%count));
			}
		    writer.write(getSVGPostfix());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer!=null)
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}		
	}
	
	private static void printLevelSVG (int[] p, ArrayList<float[]> alf, float yOffset, float xOffset, float step) {
		boolean isFinal = (2*p[0] == p.length);
		int[] occpLevels = new int[isFinal?(p.length/2):(p.length/4)];
		for (int i=0; i<p.length; i++) {
			if (p[i]<i) continue;
			int levl = 0;
			for (int j=occpLevels.length-1; j>=0; j--) {
				occpLevels[j] -= 1;
				if (occpLevels[j] <= 0) {
					occpLevels[j] = 0;
					levl = j;
				}
			}
			occpLevels [levl] = p[i]-i;
			
			float[] ff = new float[4];
			ff[0] = xOffset+step*levl; ff[1] = step*i+yOffset;
			ff[2] = xOffset+step*levl; ff[3] = step*p[i]+yOffset;
			alf.add(ff);
			
			float[] ff2 = new float[4];
			ff2[0] = xOffset-step; ff2[1] = step*i+yOffset;
			ff2[2] = xOffset+step*(2+occpLevels.length); ff2[3] = step*i+yOffset;
			alf.add(ff2);
			
			float[] ff3 = new float[4];
			ff3[0] = xOffset-step; ff3[1] = step*p[i]+yOffset;
			ff3[2] = xOffset+step*(2+occpLevels.length); ff3[3] = step*p[i]+yOffset;
			alf.add(ff3);
		}
	}
	
	private static String getAbsPathText (float[] path, boolean isClosed, String pathId) {
		String toReturn = "  <path id=\""+pathId+"\" style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" d=\"M";
		for (int i=0; i<path.length; i+=2) toReturn = toReturn + " " + (10+path[i]) + "," + (10+path[i+1]);
		toReturn = toReturn+((isClosed)?" z\"/>\n":"\"/>\n");
		return toReturn;
	}
	
	private static String getSVGPrefix(int width, int height) {
		return ""+
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"+
		"<svg\n"+
		"   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"+
		"   xmlns:cc=\"http://creativecommons.org/ns#\"\n"+
		"   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"+
		"   xmlns:svg=\"http://www.w3.org/2000/svg\"\n"+
		"   xmlns=\"http://www.w3.org/2000/svg\"\n"+
		"   version=\"1.1\"\n"+
		"   width=\""+width+"\"\n"+
		"   height=\""+height+"\">\n"+
		"  <defs/>\n";
	}
	
	private static String getSVGPostfix() {
		return ""+
		"  <metadata>\n"+
		"    <rdf:RDF>\n"+
		"      <cc:Work rdf:about=\"\">\n"+
		"        <dc:title></dc:title>\n"+
		"      </cc:Work>\n"+
		"    </rdf:RDF>\n"+
		"  </metadata>\n"+
		"</svg>";
	}
	
	private static String getLabelText(float textx, float texty, int i) {
		return "  <text xml:space=\"preserve\" style=\"font-size:10px;fill:#000000;fill-opacity:1;stroke:none;font-family:sans-serif; text-anchor:middle\" x=\""+textx+"\" y=\""+texty+"\">"+i+"</text>\n";
	}
}
