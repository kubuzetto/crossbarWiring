> **This algorithm is ported to Rust, see: [https://crates.io/crates/xbar](https://crates.io/crates/xbar)**


# wiring.java:
A locality-preserving one-sided binary tree - crossbar switch wiring design tool

![](http://oi66.tinypic.com/5ezqtz.jpg)

### What
__`wiring.java`__ is the implementation of the algorithm described in the conference paper _"[A locality preserving one-sided binary tree - crossbar switch wiring design algorithm](https://ieeexplore.ieee.org/document/7086839)"_ published in [2015 49th Annual Conference on Information Sciences and Systems (CISS)](https://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=7075844).

> __One-sided crossbar switches__ allow for a simple implementation of complete `K_n` graphs. However, designing these circuits is a cumbersome process and can be automated.
> We present an algorithm that allows designing automatic one-sided binary tree - crossbar switches which __do not exceed `floor(n/2)` columns__, and achieves `K_n` graph without connecting any wires between any three adjacent blocks, thus preserving locality in connections.

Slides of the conference presentation are provided in the `presentation.pdf` file.

### Prerequisities
#### Dependencies
There are no library dependencies.
__JDK__ is required to build and run the algorithm.
SVG output can be displayed in modern browsers; and / or modified using F/OSS rftsuch as __Inkscape__.
#### Compiling
Run `javac wiring.java` to compile.

### Usage
#### SVG output mode: `java wiring --svg N filename`
Constructs the wiring scheme for `N` inputs, draws the wiring into an SVG file. _Recommended._

#### Console output mode: `java wiring --console N`
Constructs the wiring scheme for `N` inputs, prints an ASCII representation of the wiring into `stdout`.

#### Sanity check mode: `java wiring --sanity N`
Constructs and validates __all__ the wiring schemes up to `N` inputs. Slow; used for testing the algorithm.

### Reference

Sahin, Devrim. "A locality preserving one-sided binary tree - crossbar switch wiring design algorithm." _Information Sciences and Systems (CISS), 2015 49th Annual Conference on_. IEEE, 2015.

__Bibtex__
```
@inproceedings{dsahin2015crossbar,
  title={A locality preserving one-sided binary tree - crossbar switch wiring design algorithm},
  author={{\c{S}}ahin, Devrim},
  booktitle={Information Sciences and Systems (CISS), 2015 49th Annual Conference on},
  pages={1--4},
  year={2015},
  organization={IEEE}
}
```

### License
GPLv3.
